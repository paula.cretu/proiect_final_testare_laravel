import time
import unittest
import string
import random
import select

from selenium import webdriver


class TestLaravel(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome('D:\paulac\chromedriver.exe')
        self.driver.get('https://laravel.dev-society.com/')


    def tearDown(self):
        self.driver.quit()


    def login(self, user, password):
        login_button = self.driver.find_element_by_xpath('//*[@id="login_button"]')
        login_button.click()
        time.sleep(3)

        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')
        password_button = self.driver.find_element_by_xpath(('//*[@id="password"]'))

        email_box.clear()
        password_button.clear()

        email_box.send_keys(user)
        password_button.send_keys(password)

        login_button = self.driver.find_element_by_xpath('//*[@id="login_user"]')
        login_button.click()
        time.sleep(3)


    def test_a_login(self):
        self.login('maldinutza@gmail.com', 'Paolomaldini!123')


    def test_b_login_invaliduser(self):
        self.login('maldi@ro.ro','Paolomaldini!123')
        self.assertIn('These credentials do not match our records.', self.driver.page_source)

    def test_c_login_invalidpass(self):
        self.login('maldinutza@gmail.com','Paolo')
        self.assertIn('These credentials do not match our records.', self.driver.page_source)



    def test_d_register_user(self):

        register_button=self.driver.find_element_by_xpath('//*[@id="register_button"]')
        register_button.click()
        time.sleep(5)
        self.assertIn('Register', self.driver.page_source)
        username_box= self.driver.find_element_by_xpath('//*[@id="name"]')
        email_box= self.driver.find_element_by_xpath('//*[@id="email"]')

        username_box.clear()
        email_box.clear()

        random_email=''.join(random.choices(string.ascii_lowercase, k=10))+ '@domain.com'
        random_username=''.join(random.choices(string.ascii_lowercase, k=10)) + '_' + str(random.randint(100000,999999))

        email_box.send_keys(random_email)
        username_box.send_keys(random_username)

        time.sleep(3)
        password =random.choices(string.ascii_lowercase, k=10)
        pass_button=self.driver.find_element_by_xpath('//*[@id="password"]')
        pass_button.send_keys(password)
        confirm_pass_button=self.driver.find_element_by_xpath('//*[@id="password-confirm"]')
        confirm_pass_button.send_keys(password)

        register_button = self.driver.find_element_by_xpath('//*[@id="register_account"]')
        register_button.click()

        time.sleep(3)
        self.assertIn('You have no posts yet - feel free to create one here', self.driver.page_source)


    def test_e_Addpost(self):
        self.login('maldinutza@gmail.com', 'Paolomaldini!123')
        post_button = self.driver.find_element_by_xpath('//*[@id="sidebar-posts"]/a')
        post_button.click()
        self.assertIn('post', self.driver.current_url)
        time.sleep(2)
        addanewpost_button = self.driver.find_element_by_xpath('//*[@id="create_post"]')
        addanewpost_button.click()
        time.sleep(1)
        self.assertIn('create', self.driver.current_url)
        title_box = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/input[2]')
        title_box.send_keys('Titlu')
        body_box = self.driver.find_element_by_xpath('//*[@id="text_editor"]')
        body_box.send_keys('ala bala portocala')
        time.sleep(3)
        submit_button = self.driver.find_element_by_xpath('//*[@id="submit_post"]')
        submit_button.click()
        time.sleep(2)
        self.assertIn('Post created', self.driver.page_source)
        a = str(self.driver.current_url)
        lista = a.split('/')
        TestLaravel.nrid = lista[len(lista) - 1]

        TestLaravel.xpath_edit = '//*[@id="edit-post-' + TestLaravel.nrid + '"]'
        TestLaravel.xpath_delete='//*[@id="delete-post-' + TestLaravel.nrid + '"]'


    def test_f_Edit(self):
       self.login('maldinutza@gmail.com', 'Paolomaldini!123')
       edit_button = self.driver.find_element_by_xpath(TestLaravel.xpath_edit)
       edit_button.click()
       time.sleep(2)
       titlu_modif= self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/input[2]')
       titlu_modif.clear()
       titlu_modif.send_keys('valoare noua')
       time.sleep(2)
       submit_button= self.driver.find_element_by_xpath('//*[@id="submit_post"]')
       submit_button.click()


    def test_g_DeletePost(self):
        self.login('maldinutza@gmail.com', 'Paolomaldini!123')
        DeleteButton = self.driver.find_element_by_xpath(TestLaravel.xpath_delete)
        DeleteButton.click()
        time.sleep(2)

    def test_h_modificare_settings(self):
        self.login('maldinutza@gmail.com', 'Paolomaldini!123')
        settings_button=self.driver.find_element_by_xpath('//*[@id="sidebar-settings"]/a')
        settings_button.click()
        edit_buton=self.driver.find_element_by_xpath('//*[@id="edit_account"]')
        edit_buton.click()
        time.sleep(3)
        user_name_button= self.driver.find_element_by_xpath('//*[@id="edit_name"]')
        user_name_button.clear()
        random_username = ''.join(random.choices(string.ascii_lowercase, k=10)) + '_' + str(random.randint(100000, 999999))
        user_name_button.send_keys(random_username)
        time.sleep(3)
        submit_button=self.driver.find_element_by_xpath('//*[@id="apply"]')
        submit_button.click()
        self.assertIn('User updated',self.driver.page_source)


    def test_i_register_user(self):

        register_button=self.driver.find_element_by_xpath('//*[@id="register_button"]')
        register_button.click()
        time.sleep(5)
        self.assertIn('Register', self.driver.page_source)
        username_box= self.driver.find_element_by_xpath('//*[@id="name"]')
        email_box= self.driver.find_element_by_xpath('//*[@id="email"]')

        username_box.clear()
        email_box.clear()

        random_email=''.join(random.choices(string.ascii_lowercase, k=10))+ '@domain.com'
        random_username=''.join(random.choices(string.ascii_lowercase, k=10)) + '_' + str(random.randint(100000,999999))

        email_box.send_keys(random_email)
        username_box.send_keys(random_username)

        time.sleep(3)
        password =random.choices(string.ascii_lowercase, k=10)
        pass_button=self.driver.find_element_by_xpath('//*[@id="password"]')
        pass_button.send_keys(password)
        confirm_pass_button=self.driver.find_element_by_xpath('//*[@id="password-confirm"]')
        confirm_pass_button.send_keys(password)

        register_button = self.driver.find_element_by_xpath('//*[@id="register_account"]')
        register_button.click()

        time.sleep(3)
        self.assertIn('You have no posts yet - feel free to create one here', self.driver.page_source)

        settings_button= self.driver.find_element_by_xpath('//*[@id="sidebar-settings"]/a')
        settings_button.click()
        delete_button= self.driver.find_element_by_xpath(('//*[@id="delete_button"]'))
        delete_button.click()
        self.assertIn('Login',self.driver.page_source)


















